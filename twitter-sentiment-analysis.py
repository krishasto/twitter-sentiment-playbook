!pip install apify-client
!pip install --upgrade apify-client


from apify_client import ApifyClient

# Initialize the ApifyClient with your API token
client = ApifyClient("apify_api_BT0sB2EXeaeCduNmumucnjgW15QHjI1zetlx")

# Prepare the Actor input
run_input = {
    "customMapFunction": "(object) => { return {...object} }",
    "includeSearchTerms": False,
    "maxItems": 50,
    "onlyImage": False,
    "onlyQuote": False,
    "onlyTwitterBlue": False,
    "onlyVerifiedUsers": False,
    "onlyVideo": False,
    "searchTerms": [
        "(-from:tokopediacare) AND (@tokopedia OR @tokopediacare) AND (chatbot OR robot OR \"chat bot\") filter:replies\t"
    ],
    "sort": "Latest",
    "tweetLanguage": "id"
  }

# Run the Actor and wait for it to finish
run = client.actor("61RPP7dywgiy0JPD0").call(run_input=run_input)

# Fetch and print Actor results from the run's dataset (if there are any)
for item in client.dataset(run["defaultDatasetId"]).iterate_items():
    print(item)